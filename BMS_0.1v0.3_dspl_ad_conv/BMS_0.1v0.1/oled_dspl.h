/*void disp_write(char bchstb){

int bchstb_conv = (int)bchstb;
tx_spi(bchstb_conv,DISP,DATA);
}
 * oled_dspl.h
 *
 * Created: 14.01.2016 09:57:13
 *  Author: b3
 */ 


#ifndef OLED_DSPL_H_
#define OLED_DSPL_H_

#include "spi_m0plus.h"

#define DSPL 0
#define BMS 1
#define DATA 0
#define COMMAND 1
#define FREI 0
#define BELEGT 1

#define CLR_DSPL		0x01u
#define RET_HOME		0x02u
#define SFT_RIGT		0x07u
#define SFT_LFT			0x05u
#define CURS_SFT_RIGT	0x14u
#define CURS_SFT_LFT	0x10u
#define ON_DSPL			0x0Cu


#define FUNC_SET_FUND	0x28u
#define OFF_DSPL		0x08u
#define FUNC_SET_EXT	0x09u
#define CMD_ENA_OLED	0x79u
#define CMD_DIS_OLED	0x78u

/*globale Variablen*/

/*globale Variablen ENDE*/

void dspl_clr(void){
	tx_spi(CLR_DSPL,DSPL,COMMAND);
}

void dspl_home(void){
	tx_spi(RET_HOME,DSPL,COMMAND);
}

uint8_t dspl_set_pos(int zeile, int spalte){
	
	uint8_t adress;
	
	switch(zeile){
		case 1: adress=0x00u;break;
		case 2: adress=0x20u;break;
		case 3: adress=0x40u;break;
		case 4: adress=0x60u;break;
		default: return 0;
	}
	if(spalte>20){
		return 0x80u;
	}
	else{
		adress+=spalte-1;
		adress|=(0x1u<<7);
		return adress;
	}
}
	
void dspl_init(void){
	
	//trsf_spi_disp(0x2Au);//function set (extended command set) 
	//trsf_spi_disp(0x71u);//function selection A 
	//trsf_spi_disp(0x28u);
	//data(0x00); //disable internal VDD regulator (2.8V I/O). data(0x5C) = enable regulator (5V I/O) 

tx_spi(FUNC_SET_FUND,DSPL,COMMAND);//function set (fundamental command set)
tx_spi(OFF_DSPL,DSPL,COMMAND);//display off, cursor off, blink off
tx_spi(0x2A,DSPL,COMMAND);//function set (extended command set)

/*OLED Command Set*/
tx_spi(CMD_ENA_OLED,DSPL,COMMAND);//OLED command set enabled
tx_spi(0xD5u,DSPL,COMMAND);//set display clock divide ratio/oscillator frequency
tx_spi(0xF0u,DSPL,COMMAND);//set display clock divide ratio/oscillator frequency
tx_spi(CMD_DIS_OLED,DSPL,COMMAND);//OLED command set disabled
/*OLED Command Set END*/

/*Extended Command Set*/
tx_spi(FUNC_SET_EXT,DSPL,COMMAND);//extended function set (4?lines)
tx_spi(0x06u,DSPL,COMMAND);//COM SEG direction
tx_spi(0x72u,DSPL,COMMAND);//function selection B
tx_spi(0x00u,DSPL,DATA);//RoM CGRAM selection
tx_spi(0x2A,DSPL,COMMAND);//function set (extended command set)
/*Extended Command Set END*/

/*OLED Command Set*/
tx_spi(0x79u,DSPL,COMMAND);//OLED command set enabled
tx_spi(0xDAu,DSPL,COMMAND);//set SEG pins hardware configuration
tx_spi(0x10u,DSPL,COMMAND);//set SEG pins hardware configuration
tx_spi(0xDCu,DSPL,COMMAND);//function selection C
tx_spi(0x00u,DSPL,COMMAND);//function selection C
tx_spi(0x81u,DSPL,COMMAND);//set contrast control
tx_spi(0x7Fu,DSPL,COMMAND);//set contrast control
tx_spi(0xD9u,DSPL,COMMAND);//set phase length
tx_spi(0xF1u,DSPL,COMMAND);//set phase length
tx_spi(0xDBu,DSPL,COMMAND);//set VCOMH deselect level
tx_spi(0x40u,DSPL,COMMAND);//set VCOMH deselect level
tx_spi(0x78u,DSPL,COMMAND);//OLED command set disabled
/*OLED Command Set END*/

tx_spi(0x28u,DSPL,COMMAND);//function set (fundamental command set)
tx_spi(0x01u,DSPL,COMMAND);//clear display
tx_spi(0x80u,DSPL,COMMAND);//set DDRAM address to 0x00
tx_spi(ON_DSPL,DSPL,COMMAND);//display ON

//tx_spi(0x10u,DSPL,COMMAND);//dot scroll enable
//tx_spi(0x81u,DSPL,COMMAND);//dot scroll quantity 1dot
//tx_spi(0x07u,DSPL,COMMAND);//display shift enable, right
	
//----------------Init-Ausgabe------------------
	tx_spi(0x01,DSPL,COMMAND);//clear display
 	tx_spi(0x02,DSPL,COMMAND);//return home (line 1)
 	for(int i=0;i<DSPL_MAX;i++){
	 	tx_spi(0x1F,DSPL,DATA); //write solid blocks
 	}
 	 tx_spi(0xA0,DSPL,COMMAND); //line 2
 	 for(int i=0;i<DSPL_MAX;i++){
	 	 tx_spi(0x1F,DSPL,DATA); //write solid blocks
 	 }
 	 tx_spi(0xC0,DSPL,COMMAND); //line 3
 	 for(int i=0;i<DSPL_MAX;i++){
	 	 tx_spi(0x1F,DSPL,DATA);//write solid blocks
 	 }
 	 tx_spi(0xE0,DSPL,COMMAND);//line 4
 	 for(int i=0;i<DSPL_MAX;i++){
	 	 tx_spi(0x1F,DSPL,DATA);//write solid blocks
 	 }
	

}

void dspl_write(char bchstb){
	int bchstb_conv = (int)bchstb;
	tx_spi(bchstb_conv,DSPL,DATA);
}

void dspl_satz(char *satz, int zeile, int spalte){
	
	tx_spi(dspl_set_pos(zeile, spalte),DSPL,COMMAND);
	
	int k=strlen(satz);
	if(k>20){
		k=20;
	}
	char satz_neu[k];
	strncpy(satz_neu,satz,k);
		
	for(int i=0; i<k;i++){
		dspl_write(satz_neu[i]);
	}
}

void dspl_zahl(int zahl, int zeile, int spalte){
	char var[4];
	itoa(zahl,var,10);
	dspl_satz(var,zeile,spalte);
}


#endif /* OLED_DSPL_H_ */