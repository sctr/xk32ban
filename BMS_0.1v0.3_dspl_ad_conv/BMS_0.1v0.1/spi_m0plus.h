/*
 * spi_m0plus.h
 *
 * Created: 14.01.2016 10:12:15
 *  Author: b3
 */ 


#ifndef SPI_M0PLUS_H_
#define SPI_M0PLUS_H_

#include "names_bms.h"

#define DSPL 0
#define BMS 1
#define DATA 0
#define COMMAND 1
#define FREI 0
#define BELEGT 1


#define EXIT_FAILURE -1

int stat_spi=BELEGT;

int tx_spi(uint32_t cmd, uint8_t slave, uint8_t dc){

F_PIOA_OUT|=(0x1u<<LED_STAT0);
	uint8_t cmd_bms[4];
	int stat_spi=BELEGT;
	uint8_t cmd0=cmd&(~((1<<4)|(1<<5)|(1<<6)|(1<<7)));//linke Bits auf Null stellen
	uint8_t cmd1=cmd>>4;//linke Bits auf Null stellen, indem alle Bits um 4 Stellen nach rechts verschoben werden (Rest wird mit Nullen aufgef�llt)

	//-----------soll Display oder BMS angesprochen werden----------------------
	if(slave==DSPL){
		//reinit spi
		F_SERC_CTRLA&=~(0x1u<<1);//disable spi
		F_SERC_CTRLA|=(0x1u<<29);//CPOL f�r SPI Mode 3 (Cpha bleibt auf 1)
		F_SERC_CTRLA|= (0x1u<<30);	//LSB zuerst
		F_SERC_CTRLA|=(0x1u<<1);//enable spi
		F_PIOA_OUT&=~(0x1u<<8);//Display zur Slave Auswahl auf Ground ziehen
		
	//-----------------------ist es ein Kommando oder eine Datenfolge--------------------- DATA/COMMAND
	if (dc==COMMAND){
		F_SERC_DATA=0x1Fu; //Start Daten zu �bertragen(0xF8u f�r StartByte), (danach warten auf das Interrupt, was die erfolgreiche �bersendung best�tigt)
		for(int k=0;k<SPI_DSPL_WAIT;k++);
		while((F_SERC_INTFLAG & (0x1u<<1))){//auf positive R�ckmeldung warten
			F_SERC_INTFLAG|=(0x1u<<1); //Interruptflag clearen
		}
	}
	else if(dc==DATA){
		F_SERC_DATA=0x5Fu; //Start Daten zu �bertragen(0xFAu f�r StartByte), (danach warten auf das Interrupt, was die erfolgreiche �bersendung best�tigt)
		for(int k=0;k<SPI_DSPL_WAIT;k++);
		while((F_SERC_INTFLAG & (0x1u<<1))){//auf positive R�ckmeldung warten
			F_SERC_INTFLAG|=(0x1u<<1); //Interruptflag clearen
		}
	}
	else{
		return EXIT_FAILURE;
	}

	//-----------------�bertrage Daten/Commando--------------------------------------

	F_SERC_DATA=cmd0; //Start Daten zu �bertragen, danach warten auf das Interrupt, was die erfolgreiche �bersendung best�tigt
	for(int k=0;k<SPI_DSPL_WAIT;k++);
	while((F_SERC_INTFLAG & (0x1u<<1))){//auf positive R�ckmeldung warten
		F_SERC_INTFLAG|=(0x1u<<1); //Interruptflag clearen
	}

	F_SERC_DATA=cmd1; //Start Daten zu �bertragen, danach warten auf das Interrupt, was die erfolgreiche �bersendung best�tigt
	for(int k=0;k<SPI_DSPL_WAIT;k++);
	while((F_SERC_INTFLAG & (0x1u<<1))){//auf positive R�ckmeldung warten
		F_SERC_INTFLAG|=(0x1u<<1); //Interruptflag clearen
	}
			}
	else if(slave==BMS){
		F_SERC_CTRLA&=~(0x1u<<1);	//SPI zur Konfiguration ausschalten
		F_SERC_CTRLA&=~(0x1u<<29);	//CPOL f�r SPI Mode 1 (Cpha bleibt auf 1)
		F_SERC_CTRLA&=~(0x1u<<30);	//MSB zuerst
		F_SERC_CTRLA|=(0x1u<<1);	//SPI einschalten
		F_PIOA_OUT&=~(0x1u<<6);		//BMS zur Slave Auswahl auf Ground ziehen
						
			cmd_bms[0]=(cmd>>24)&0x0F;
			cmd_bms[1]=(cmd>>16);	
			cmd_bms[2]=(cmd>>8);
			cmd_bms[3]=cmd;
			
				for (int i=0; i<4;i++){
					F_SERC_DATA=cmd_bms[i];
						for(int k=0;k<SPI_DSPL_WAIT;k++);
					while((F_SERC_INTFLAG & (0x1u<<1))){	//auf positive R�ckmeldung warten
					F_SERC_INTFLAG|=(0x1u<<1);				//Interruptflag clearen
					}	
				}
			
			
	}
	else{
		stat_spi=0;
		return 0;
	}

	
	F_PIOA_OUT|=(0x1u<<8)|(0x1u<<6);
	F_PIOA_OUT&=~(0x1u<<LED_STAT0);
	stat_spi=0;
}

/**
 * \brief 
 * vorzugsweise zum Dummy Readout von AD7820 (Falsche 32Bit Daten
 * werden gesendet, um zeitgleich Daten zu erhalten)
 * \param cmd
 * \param slave
 * \param dc
 * 
 * \return int
 */
int trx_spi(uint32_t cmd, uint8_t slave, uint8_t dc){

	F_PIOA_OUT|=(0x1u<<LED_STAT0);
	uint8_t cmd_bms[4];
	uint32_t rcv_bms32=0x00000000;
		
	
	int stat_spi=BELEGT;
	
	if(slave==BMS){
		F_SERC_CTRLA&=~(0x1u<<1);	//SPI zur Konfiguration ausschalten
		F_SERC_CTRLA&=~(0x1u<<29);	//CPOL f�r SPI Mode 1 (Cpha bleibt auf 1)
		F_SERC_CTRLA&=~(0x1u<<30);	//MSB zuerst
		F_SERC_CTRLA|=(0x1u<<1);	//SPI einschalten
		F_PIOA_OUT&=~(0x1u<<6);		//BMS zur Slave Auswahl auf Ground ziehen
		
		cmd_bms[0]=cmd;
		cmd_bms[1]=(cmd>>8);
		cmd_bms[2]=(cmd>>16);
		cmd_bms[3]=(cmd>>24);
				
		for (int i=3; i>-1;i--){
			F_SERC_DATA=cmd_bms[i];
			for(int k=0;k<SPI_DSPL_WAIT;k++);
			while((F_SERC_INTFLAG & (0x1u<<1))){	//auf positive R�ckmeldung warten
								F_SERC_INTFLAG|=(0x1u<<1);				//Interruptflag clearen
			}
				rcv_bms32|=(F_SERC_DATA<<(i*8));
		}
		
	}
	else{
		stat_spi=0;
		return 0;
	}

	
	F_PIOA_OUT|=(0x1u<<8)|(0x1u<<6);
	F_PIOA_OUT&=~(0x1u<<LED_STAT0);
	stat_spi=0;
	return rcv_bms32;
}

uint32_t trx_spi_cnvst(uint32_t cmd, uint8_t slave, uint8_t dc){

	F_PIOA_OUT|=(0x1u<<LED_STAT0);
	uint8_t cmd_bms[4];
	uint32_t rcv_bms32=0x00000000;
	
	int stat_spi=BELEGT;
	
	if(slave==BMS){
		F_SERC_CTRLA&=~(0x1u<<1);	//SPI zur Konfiguration ausschalten
		F_SERC_CTRLA&=~(0x1u<<29);	//CPOL f�r SPI Mode 1 (Cpha bleibt auf 1)
		F_SERC_CTRLA&=~(0x1u<<30);	//MSB zuerst
		F_SERC_CTRLA|=(0x1u<<1);	//SPI einschalten
		F_PIOA_OUT&=~(0x1u<<6);		//BMS zur Slave Auswahl auf Ground ziehen
		
		cmd_bms[0]=cmd;
		cmd_bms[1]=(cmd>>8);
		cmd_bms[2]=(cmd>>16);
		cmd_bms[3]=(cmd>>24);
				
		for (int i=3; i>-1;i--){
			F_SERC_DATA=cmd_bms[i];
			for(int k=0;k<SPI_DSPL_WAIT;k++);
			while((F_SERC_INTFLAG & (0x1u<<1))){	//auf positive R�ckmeldung warten
								F_SERC_INTFLAG|=(0x1u<<1);				//Interruptflag clearen
			}
				rcv_bms32|=(F_SERC_DATA<<(i*8));
		}
				
	}
	else{
		stat_spi=0;
		return 0;
	}

	
	F_PIOA_OUT|=(0x1u<<8)|(0x1u<<6);
	F_PIOA_OUT&=~(0x1u<<LED_STAT0);
	stat_spi=0;
	return rcv_bms32;
}

void bms_write(uint8_t device, uint8_t adr, uint8_t dat, bool adr_all){
	
	//STATUS LED AN STELLEN
	F_PIOA_OUT|=(0x1u<<LED_STAT0);
	//STATUS LED AN STELLEN ENDE
		
	//KOMMANDO VARIABLEN ERSTELLEN + EINZELKOMMANDOS IN �BERTRAGUNG EINPFLEGEN
	uint32_t cmd=0x00000000;
	uint8_t bt_ptrn=0x2;
	cmd|=((device&0x1F)<<27);
	cmd|=((adr&0x3F)<<21);
	cmd|=((dat&0xFF)<<13);
	cmd|=((adr_all&0x1)<<12);
	cmd|=((bt_ptrn&0x3)<<0);
	//KOMMANDO VARIABLEN ERSTELLEN + EINZELKOMMANDOS IN �BERTRAGUNG EINPFLEGEN ENDE
	
	//EMPFANGS VARIABLEN ERSTELLEN
	uint8_t cmd_bms[4];
	uint32_t rcv_bms32=0x00000000;
	uint8_t rcv_dev=0x0000;
	uint8_t rcv_ch=0x00;
	uint16_t rcv_dat=0x0000;
	bool rcv_ack=0;
	//EMPFANGS VARIABLEN ERSTELLEN ENDE
	
	//CRC VARIABLEN
	uint8_t num_bits_w=21;
	int xor_1=0, xor_2=0, xor_3=0, xor_4=0, xor_5=0;
	bool data_in[20];
	int crc_0=0, crc_1=0, crc_2=0, crc_3=0, crc_4=0, crc_5=0, crc_6=0, crc_7=0;
	
	//CRC VARIABLEN ENDE
	
	//CRC BERECHNUNG
	for(int i=0; i<22; i++){
		data_in[i]=(cmd>>(i+11))&0x1;
	}
	
	for(int i=num_bits_w; i>=0;i--){
		xor_5=crc_4^crc_7;
		xor_4=crc_2^crc_7;
		xor_3=crc_1^crc_7;
		xor_2=crc_0^crc_7;
		xor_1=data_in[i]^crc_7;
		
		crc_7=crc_6;
		crc_6=crc_5;
		crc_5=xor_5;
		crc_4=crc_3;
		crc_3=xor_4;
		crc_2=xor_3;
		crc_1=xor_2;
		crc_0=xor_1;
	}
	//CRC BERECHNUNG ENDE
	
	//CRC IN cmd Variable �ERTRAGEN
	cmd|=((crc_0&0x1)<<3);
	cmd|=((crc_1&0x1)<<4);
	cmd|=((crc_2&0x1)<<5);
	cmd|=((crc_3&0x1)<<6);
	cmd|=((crc_4&0x1)<<7);
	cmd|=((crc_5&0x1)<<8);
	cmd|=((crc_6&0x1)<<9);
	cmd|=((crc_7&0x1)<<10);
	//CRC IN cmd Variable �ERTRAGEN ENDE
	
	//32BIT KOMMANDO VARIABLE F�R �BERTRAGUNG IN 4 8BIT VARIABLEN AUFTEILEN
	cmd_bms[0]=cmd;
	cmd_bms[1]=(cmd>>8);
	cmd_bms[2]=(cmd>>16);
	cmd_bms[3]=(cmd>>24);
	//32BIT KOMMANDO VARIABLE F�R �BERTRAGUNG IN 4 8BIT VARIABLEN AUFTEILEN ENDE
	
	//�BERTRAGUNG BEGINNEN
	
		F_SERC_CTRLA&=~(0x1u<<1);	//SPI zur Konfiguration ausschalten
		F_SERC_CTRLA&=~(0x1u<<29);	//CPOL f�r SPI Mode 1 (Cpha bleibt auf 1)
		F_SERC_CTRLA&=~(0x1u<<30);	//MSB zuerst
		F_SERC_CTRLA|=(0x1u<<1);	//SPI einschalten
		F_PIOA_OUT&=~(0x1u<<6);		//BMS zur Slave Auswahl auf Ground ziehen
				
		for (int i=3; i>-1;i--){
			F_SERC_DATA=cmd_bms[i];
			for(int k=0;k<SPI_DSPL_WAIT;k++);
			while((F_SERC_INTFLAG & (0x1u<<1))){	//auf positive R�ckmeldung warten
				F_SERC_INTFLAG|=(0x1u<<1);				//Interruptflag clearen
			}
		}
		
	//�BERTRAGUNG BEGINNEN ENDE
	
	//_SS AUFHEBEN UND STATUSLED AUS
	F_PIOA_OUT|=(0x1u<<8)|(0x1u<<6);
	F_PIOA_OUT&=~(0x1u<<LED_STAT0);
	//_SS AUFHEBEN UND STATUSLED AUS ENDE
	
}

int bms_read(uint8_t dev, uint8_t adr){
	
	//STATUS LED AN STELLEN
	F_PIOA_OUT|=(0x1u<<LED_STAT0);
	//STATUS LED AN STELLEN ENDE
		
	//KOMMANDO VARIABLEN ERSTELLEN + EINZELKOMMANDOS IN �BERTRAGUNG EINPFLEGEN
	uint32_t cmd=0x00000000;
	bool adr_all=0x0; //um alle zu adressieren-->0x1, dann m�ssen auch mehrere readbacks erledigt werden
	uint8_t bt_ptrn=0x2;
	cmd|=((dev&0x1F)<<27);
	cmd|=((READ&0x3F)<<21);
	cmd|=(((adr<<2)&0xFF)<<13);
	cmd|=((adr_all&0x1)<<12);
	cmd|=((bt_ptrn&0x3)<<0);
	//KOMMANDO VARIABLEN ERSTELLEN + EINZELKOMMANDOS IN �BERTRAGUNG EINPFLEGEN ENDE
	
	//EMPFANGS VARIABLEN ERSTELLEN
	uint8_t cmd_bms[4];
	uint32_t rcv_bms32=0x00000000;
	uint8_t rcv_dev=0x0000;
	uint8_t rcv_ch=0x00;
	uint16_t rcv_dat=0x0000;
	bool rcv_ack=0;
	//EMPFANGS VARIABLEN ERSTELLEN ENDE
	
	//CRC VARIABLEN
	uint8_t num_bits_w=21;
	int xor_1=0, xor_2=0, xor_3=0, xor_4=0, xor_5=0;
	bool data_in[20];
	int crc_0=0, crc_1=0, crc_2=0, crc_3=0, crc_4=0, crc_5=0, crc_6=0, crc_7=0;
	//CRC VARIABLEN ENDE
	
	//CRC BERECHNUNG
	for(int i=0; i<22; i++){
		data_in[i]=(cmd>>(i+11))&0x1;
	}
	
	for(int i=num_bits_w; i>=0;i--){
		xor_5=crc_4^crc_7;
		xor_4=crc_2^crc_7;
		xor_3=crc_1^crc_7;
		xor_2=crc_0^crc_7;
		xor_1=data_in[i]^crc_7;
		
		crc_7=crc_6;
		crc_6=crc_5;
		crc_5=xor_5;
		crc_4=crc_3;
		crc_3=xor_4;
		crc_2=xor_3;
		crc_1=xor_2;
		crc_0=xor_1;
	}
	//CRC BERECHNUNG ENDE
	
	//CRC IN cmd Variable �ERTRAGEN
	cmd|=((crc_0&0x1)<<3);
	cmd|=((crc_1&0x1)<<4);
	cmd|=((crc_2&0x1)<<5);
	cmd|=((crc_3&0x1)<<6);
	cmd|=((crc_4&0x1)<<7);
	cmd|=((crc_5&0x1)<<8);
	cmd|=((crc_6&0x1)<<9);
	cmd|=((crc_7&0x1)<<10);
	//CRC IN cmd Variable �ERTRAGEN ENDE
	
	//32BIT KOMMANDO VARIABLE F�R �BERTRAGUNG IN 4 8BIT VARIABLEN AUFTEILEN
	cmd_bms[0]=cmd;
	cmd_bms[1]=(cmd>>8);
	cmd_bms[2]=(cmd>>16);
	cmd_bms[3]=(cmd>>24);
	//32BIT KOMMANDO VARIABLE F�R �BERTRAGUNG IN 4 8BIT VARIABLEN AUFTEILEN ENDE
	
	//�BERTRAGUNG BEGINNEN
		F_SERC_CTRLA&=~(0x1u<<1);	//SPI zur Konfiguration ausschalten
		F_SERC_CTRLA&=~(0x1u<<29);	//CPOL f�r SPI Mode 1 (Cpha bleibt auf 1)
		F_SERC_CTRLA&=~(0x1u<<30);	//MSB zuerst
		F_SERC_CTRLA|=(0x1u<<1);	//SPI einschalten
		F_PIOA_OUT&=~(0x1u<<6);		//BMS zur Slave Auswahl auf Ground ziehen
				
		for (int i=3; i>-1;i--){
			F_SERC_DATA=cmd_bms[i];
			for(int k=0;k<SPI_DSPL_WAIT;k++);
			while((F_SERC_INTFLAG & (0x1u<<1))){	//auf positive R�ckmeldung warten
				F_SERC_INTFLAG|=(0x1u<<1);				//Interruptflag clearen
			}
			rcv_bms32|=(F_SERC_DATA<<(i*8));
		}
		F_PIOA_OUT|=(0x1u<<6);		//BMS zur Slave Disauswahl hoch ziehen
	//�BERTRAGUNG BEGINNEN ENDE
	
	//PSEUDO32BIT KOMMANDO VARIABLE F�R �BERTRAGUNG IN 4 8BIT VARIABLEN AUFTEILEN
	cmd_bms[0]=0xF800030A;
	cmd_bms[1]=(0xF800030A>>8);
	cmd_bms[2]=(0xF800030A>>16);
	cmd_bms[3]=(0xF800030A>>24);
	//PSEUDO32BIT KOMMANDO VARIABLE F�R �BERTRAGUNG IN 4 8BIT VARIABLEN AUFTEILEN ENDE
	
	F_SERC_CTRLA&=~(0x1u<<1);	//SPI zur Konfiguration ausschalten
		F_SERC_CTRLA&=~(0x1u<<29);	//CPOL f�r SPI Mode 1 (Cpha bleibt auf 1)
		F_SERC_CTRLA&=~(0x1u<<30);	//MSB zuerst
		F_SERC_CTRLA|=(0x1u<<1);	//SPI einschalten
		F_PIOA_OUT&=~(0x1u<<6);		//BMS zur Slave Auswahl auf Ground ziehen
	
	//PSEUDO�BERTRAGUNG BEGINNEN
		
		for (int i=3; i>-1;i--){
			F_SERC_DATA=cmd_bms[i];
			for(int k=0;k<SPI_DSPL_WAIT;k++);
			while((F_SERC_INTFLAG & (0x1u<<1))){		//auf positive R�ckmeldung warten
				F_SERC_INTFLAG|=(0x1u<<1);				//Interruptflag clearen
			}
			rcv_bms32|=(F_SERC_DATA<<(i*8));
		}
	//PSEUDO�BERTRAGUNG BEGINNEN ENDE
	
	//_SS AUFHEBEN UND STATUSLED AUS
	F_PIOA_OUT|=(0x1u<<8)|(0x1u<<6);
	F_PIOA_OUT&=~(0x1u<<LED_STAT0);
	//_SS AUFHEBEN UND STATUSLED AUS ENDE
	
	//DATA AUS READBACK AUSLESEN UND ZUR�CKGEBEN
	rcv_ack=(rcv_bms32>>10)&0x1;
	if(rcv_ack){
		return (rcv_bms32>>13)&0xFF;
	}
	else{
		return -1;
	}
	//DATA AUS READBACK AUSLESEN UND ZUR�CKGEBEN
}


#endif /* SPI_M0PLUS_H_ */