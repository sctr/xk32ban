/*
 * BMS_0.1v0.1.c
 *
 * Created: 20.11.2015 21:18:48
 * Author : Scofield
 */ 

#include "system_samd10.h"
#include <samd10d14am.h>


/*Define Uhren*/
#define F_GCLK_CTRL		*((volatile unsigned char *) 0x40000C00)
#define F_GCLK_STATUS	*((volatile unsigned char *) 0x40000C01)
#define F_GCLK_CLKCTRL	*((volatile unsigned short int *) 0x40000C02)
#define F_GCLK_GENCTRL	*((volatile unsigned int *) 0x40000C04)
#define F_GCLK_GENDIV	*((volatile unsigned int *) 0x40000C08)

/*Define PortA*/
#define F_PIOA_DIR		*((volatile unsigned int *) 0x41004400)
#define F_PIOA_DIRCLR	*((volatile unsigned int *) 0x41004404)
#define F_PIOA_DIRSET	*((volatile unsigned int *) 0x41004408)
#define F_PIOA_DIRTGL	*((volatile unsigned int *) 0x4100440C)
#define F_PIOA_OUT		*((volatile unsigned int *) 0x41004410)
#define F_PIOA_OUTCLR	*((volatile unsigned int *) 0x41004414)
#define F_PIOA_OUTSET	*((volatile unsigned int *) 0x41004418)
#define F_PIOA_OUTTGL	*((volatile unsigned int *) 0x4100441C)
#define F_PIOA_IN		*((volatile unsigned int *) 0x41004420)
#define F_PIOA_CTRL		*((volatile unsigned int *) 0x41004424)
#define F_PIOA_WRCONFIG	*((volatile unsigned int *) 0x41004428)

#define F_PIOA_PINCFG10	*((volatile unsigned int *) 0x4100444A)
#define F_PIOA_PINCFG11	*((volatile unsigned int *) 0x4100444B)
#define F_PIOA_PINCFG22	*((volatile unsigned int *) 0x41004456)
#define F_PIOA_PINCFG23	*((volatile unsigned int *) 0x41004457)

/*Pin Konfiguration*/

#define F_PIOA_PINCFG16	*((volatile unsigned int *) 0x41004450) /*Push2*/
#define F_PIOA_PINCFG15	*((volatile unsigned int *) 0x4100444F)/*Push1*/
#define F_PIOA_PINCFG14	*((volatile unsigned int *) 0x4100444E)/*Push0*/
													

/*Testzwecke*/
#define F_PIOA_DIR6		*((volatile unsigned int *) 0x60000000)
#define F_PIOA_OUT6		*((volatile unsigned int *) 0x60000010)
#define F_PM_EXTCTRL	*((volatile unsigned int *) 0x40000402)

/*PAC Register*/
#define F_PAC_WPCLR0		*((volatile unsigned int *) 0x40000000)
#define F_PAC_WPCLR1		*((volatile unsigned int *) 0x41000000)
#define F_PAC_WPCLR2		*((volatile unsigned int *) 0x42000000)

/*EIC Kontrolle*/

#define F_EIC_CTRL			*((volatile unsigned int *) 0x40001800)
#define F_EIC_STATUS		*((volatile unsigned int *) 0x40001801)
#define F_EIC_NMICTRL		*((volatile unsigned int *) 0x40001802)
#define F_EIC_NMIFLAG		*((volatile unsigned int *) 0x40001803)
#define F_EIC_EVCTRL		*((volatile unsigned int *) 0x40001804)
#define F_EIC_INTENCLR		*((volatile unsigned int *) 0x40001808)
#define F_EIC_INTENSET		*((volatile unsigned int *) 0x4000180C)
#define F_EIC_INTFLAG		*((volatile unsigned int *) 0x40001810)
#define F_EIC_WAKEUP		*((volatile unsigned int *) 0x40001814)
#define F_EIC_CONFIG		*((volatile unsigned int *) 0x40001818)

#define F_NVIC_ISER			*((volatile unsigned int *) 0xE000E100)

void EIC_Handler(void){
F_PIOA_OUTTGL=0x00c00c00;

F_EIC_INTFLAG|=(0x1<<0)|(0x1<<1);
}
void NMI_Handler(void){
	F_PIOA_OUTTGL=0x00c00c00;
F_EIC_NMIFLAG|(0x1<<0);
}

int main(void)
{
	
	
	/*Uhren ein schalten*/
	F_GCLK_GENDIV=0x00000000;
	F_GCLK_GENCTRL=0x00010600;
	F_GCLK_CLKCTRL=0x4005; /*Generische Uhr (OSC8M) aktivieren, Generische Uhr erlauben*/
	//F_NVIC_ISER=0xFFFFFFFF; /*Setze alle Interrupts frei*/
	F_NVIC_ISER=0x00000010; /*Setze EICi Interrupts frei*/
		
	/*PORTA Initialisieren*/
	/*PIN 10,11,22,23 f�r LED als sink Ausgang konfigurieren*/
    F_PIOA_DIR|=(0x1<<10)|(0x1<<11)|(0x1<<22)|(0x1<<23);
					
	/*Pin 16 als Eingang konfigurieren*/
	F_PIOA_DIR &= ~(1<<14);
	F_PIOA_DIR &= ~(1<<15);
	F_PIOA_DIR &= ~(1<<16);
	F_PIOA_OUTSET|= (1<<14);
	F_PIOA_OUTSET|= (1<<15);
	F_PIOA_OUTSET|= (1<<16);
	/*Einstellungen der Pins werden mit einer Pinmaske gesetzt 
	Pinmasken sind Halbw�rter, d.h. (1<<31) selektiert die oberen 16 Pins
	~&(1<<31) selektiert die unteren 16 Pins.
	ACHTUNG: Die Pins k�nnen nur beschrieben werden wenn (1<<30)Write PINCFG
	F_PIOA_PINCFG werden nicht beschrieben, das erledigt die Pinmask von WRCONFIG*/
	
	F_PIOA_WRCONFIG|=(1<<14)|(1<<15)|(1<<16)|(1<<17)|(1<<18)|(1<<30);
	F_PIOA_WRCONFIG|=(1<<0)|(1<<16)|(1<<17)|(1<<18)|(1<<30)|(1<<31);
		
	/*Variablen*/
	int i = 0;
	int zeit = 100;
	/*EIC Initialisieren*/
	F_EIC_CONFIG|=(0x2<<0)|(0x2<<4);/*Fallende Flanken Pr�fung, Keine Filter*/
	F_EIC_INTENSET|=(0x1<<0)|(0x1<<1);/*EXTINT 0 und 1 an Pin 15 und 16 als externen Interrupt gesetzt*/
	
	
	/*NMI Initialisieren*/
	//F_EIC_NMICTRL|=(0x2<<0);/*Fallende Flanken Pr�fung, Keine Filter*/
	
	/*EICi erlauben*/
	F_EIC_CTRL|=(0x1<<1);

 
      /* Replace with your application code */
    while (1) 
    {

    //F_PIOA_OUTTGL=0x00c00c00;
    //for(i=0; i<=zeit; i++);
    //F_PIOA_OUTTGL=0x00000c00;
    //for(i=0; i<=zeit; i++);
    //F_PIOA_OUTTGL=0x00000c00;
    //for(i=0; i<=zeit; i++);
    //F_PIOA_OUTTGL=0x00c00000;
    //for(i=0; i<=zeit; i++);
		
    }
}
