/*
Dieses Programm steuert vier IGBTs f�r das Betreiben eines Gleichstrommotors
Es soll ein Potentiometer abgefragt werden, der das Vor- und R�ckw�rtsfahren steuert
Der Potentiometer wird mit einem ADCWander am uC ermittelt und zur Verarbeitung 
weitergegeben.
Die ermittelten Daten werden an die PWM Steuereinheit weiter gegeben. Durch die
PWM werden vier Optokoppler betrieben. An ihren Ausg�ngen werden Treiber f�r vier IGBTs
betrieben.
Jeweils zwei PWM Ausg�nge k�nnen zu einem zusammengefasst, bzw. mit der gleichen Funktion
angetrieben werden.
 */ 

/*Preprocessor*/
/*ADC Processor*/

#define TRUE 1
#define FALSE 0

#define F_ADC_CHER	*((volatile unsigned int *) 0x40038010)
#define F_ADC_CR	*((volatile unsigned int *) 0x40038000)
#define F_PMC_PCER0	*((volatile unsigned int *) 0x400E0410)
#define F_ADC_CDR6	*((volatile unsigned int *) 0x40038068)

#define CH6 (0x1u<<6)
#define START (0x1u<<1)
#define PID29 (0x1u<<29)

/*PWM Processor*/
#define F_PIOA_ABCDSR1	*((volatile unsigned int *) 0x400E0E70)
#define F_PWM_WPCR		*((volatile unsigned int *) 0x400200E4)
#define F_PWM_CLK		*((volatile unsigned int *) 0x40020000)
#define F_PWM_SCM		*((volatile unsigned int *) 0x40020020)
#define F_PWM_CMR0		*((volatile unsigned int *) 0x40020200)
#define F_PWM_CMR1		*((volatile unsigned int *) 0x40020220)
#define F_PWM_CMR2		*((volatile unsigned int *) 0x40020240)
#define F_PWM_CMR3		*((volatile unsigned int *) 0x40020260)
#define F_PWM_ENA		*((volatile unsigned int *) 0x40020004)
#define F_PWM_DIS		*((volatile unsigned int *) 0x40020008)
#define F_PWM_SR		*((volatile unsigned int *) 0x4002000C)
#define F_PWM_CPRD0		*((volatile unsigned int *) 0x400200E4)
#define F_PWM_CPRD1		*((volatile unsigned int *) 0x4002022C)
#define F_PWM_CPRD2		*((volatile unsigned int *) 0x4002024C)
#define F_PWM_CPRD3		*((volatile unsigned int *) 0x4002026C)
#define F_PWM_CDTY0		*((volatile unsigned int *) 0x40020204)
#define F_PWM_CDTY1		*((volatile unsigned int *) 0x40020224)
#define F_PWM_CDTY2		*((volatile unsigned int *) 0x40020244)
#define F_PWM_CDTY3		*((volatile unsigned int *) 0x40020264)
#define F_PWM_CPRDUPD0	*((volatile unsigned int *) 0x40020210)
#define F_PWM_CDTYUPD0	*((volatile unsigned int *) 0x40020208)
#define F_PWM_CPRDUPD1	*((volatile unsigned int *) 0x40020230)
#define F_PWM_CDTYUPD1	*((volatile unsigned int *) 0x40020228)
#define F_PWM_CPRDUPD2	*((volatile unsigned int *) 0x40020250)
#define F_PWM_CDTYUPD2	*((volatile unsigned int *) 0x40020248)
#define F_PWM_CPRDUPD3	*((volatile unsigned int *) 0x40020270)
#define F_PWM_CDTYUPD3	*((volatile unsigned int *) 0x40020268)
#define F_PWM_CMPV0		*((volatile unsigned int *) 0x40020130)
#define F_PWM_IER1		*((volatile unsigned int *) 0x40020010)
#define F_PWM_IER2		*((volatile unsigned int *) 0x4002003C)
#define F_PWM_IDR1		*((volatile unsigned int *) 0x40020014)
#define F_PWM_IDR2		*((volatile unsigned int *) 0x40020038)
#define F_PWM_SCUC		*((volatile unsigned int *) 0x40020028)




#define WPCMD (0x1u<<1)
#define WPRG0 (0x1u<<2)
#define WPRG1 (0x1u<<3)
#define WPRG2 (0x1u<<4)
#define WPRG3 (0x1u<<5)
#define WPRG4 (0x1u<<6)
#define WPRG5 (0x1u<<7)
#define WPKEY (0x50574D<<8)

#define DIVA1 (0x1u<<0)
#define DIVB1 (0x1u<<16)
#define PREA (0x1u<<8)
#define PREB (0x1u<<24)

#define CPRE0 (0x1u<<0) /*MSTR CLK mit &~*/
#define CPRE1 (0x3u<<0) /*CLK:2*/
#define CALG (0x1u<<8) /*Channel alignment*/
#define CPOL (0x1u<<9) /*Channel polarity*/

#define CHID0 (0x1u<<0) /*Channel ID en-/disable*/
#define CHID1 (0x1u<<1)
#define CHID2 (0x1u<<2)
#define CHID3 (0x1u<<3)

#define UPDM0 (0x1u<<16) /**/
#define UPDM1 (0x1u<<16) /*Update mode for sync. ch. manually*/
#define UPDM2 (0x3u<<16) /*Update mode for sync. ch. automatically*/
#define PTRM (0x1u<<20) /*Corresponding flag, when periodic time elapse*/

#define SYNC0 (0x1u<<0) /*Synchronous Channels*/
#define SYNC1 (0x1u<<1)
#define SYNC2 (0x1u<<2)
#define SYNC3 (0x1u<<3)

#define FCHID0 (0x1u<<16)
#define FCHID1 (0x1u<<17)
#define FCHID2 (0x1u<<18)
#define FCHID3 (0x1u<<19)

#define WRDY (0x1u<<0)
#define UPDULOCK (0x1u<<0)/*Update of duty cycle, period, and death time of PWM in F_PWM_SCUC reg.*/

/*PMC Preprozessor*/

#define F_PMC_PCER0 *((volatile unsigned int *) 0x400E0410)

/*PIO Preprozessor*/

#define F_PIOA_PDR *((volatile unsigned int *) 0x400E0E04)

/*WDT Preprozessor*/

#define F_WDT_CR *((volatile unsigned int *) 0x400E1450)

/*PreFunctions*/
void adc(void);
void adc_init(void);
void adc_start_single(void);
int adc_conversion_read(void);

void pwm(void);

/*Functions*/
void adc(void){
	F_PMC_PCER0|= PID29;
	F_ADC_CHER|= CH6;
	F_ADC_CR|= START;
	
};
void adc_init(void){
	F_PMC_PCER0|= PID29;
	F_ADC_CHER|= CH6;
};
void adc_start_single(void){
	F_ADC_CR|= START;
};
int adc_conversion_read(void){
	return F_ADC_CDR6;
}
void pwm(void){
		
	/*<>PREQUISITES*/
	F_PMC_PCER0|=(0x1u<<31);/*Enables PMC Peripheral PWM Clock due to PID31*/
	F_PIOA_PDR|=0xffffffff;/*Disables PIOs --> enables peripheral functions*/
	F_PIOA_ABCDSR1 =(0x1u<<11)|(0x7u<<12);/*Peripheral A selected for PA11-14-->PWMChannels*/
	/*</>PREQUISITES*/
	
	/*Unlock User Interface by writing the WPCMD field in the PWM_WPCR*/
	F_PWM_WPCR=0x50574DFC;
	/*PWM->PWM_WPCR|= WPRG0|WPRG1|WPRG2|WPRG3|WPRG4|WPRG5;
	PWM->PWM_WPCR&= ~WPCMD &~WPKEY;*/
	/*Configuration of the clock generator (DIVA, PREA, DIVB, PREB in the PWM_CLK register if required)*/
	F_PWM_CLK|=DIVA1|DIVB1;
	F_PWM_CLK|=PREA |PREB;
	/*Selection of the clock for each channel (CPRE field in PWM_CMRx)*/
	F_PWM_CMR0&=~CPRE0;
	F_PWM_CMR1&=~CPRE0;
	F_PWM_CMR2&=~CPRE0;
	F_PWM_CMR3&=~CPRE0;
	/*Configuration of the waveform alignment for each channel (CALG field in PWM_CMRx)*/
	/*Configuration of the output waveform polarity for each channel (CPOL bit in PWM_CMRx)*/
	 F_PWM_CMR0|=CALG;
	 F_PWM_CMR1|=CALG;
	 F_PWM_CMR2|=CALG;
	 F_PWM_CMR3|=CALG;
	/*Selection of the counter event selection (if CALG = 1) for each channel (CES field in PWM_CMRx)*/
 	/*Configuration of the period for each channel (CPRD in the PWM_CPRDx register). Writing in PWM_CPRDx
 	register is possible while the channel is disabled. After validation of the channel, the user must use
 	PWM_CPRDUPDx register to update PWM_CPRDx as explained below*/
	 /*This register can only be written if bits WPSWS3 and WPHWS3 are cleared in the �PWM Write Protection Status Register�*/
	 F_PWM_DIS|=CHID0 |CHID1 |CHID2 |CHID3;
	 F_PWM_CPRD0|=0x0fffu;
	 F_PWM_CPRD1|=0x0fffu;
	 F_PWM_CPRD2|=0x0fffu;
	 F_PWM_CPRD3|=0x0fffu;
	 
	 /*Configuration of the duty-cycle for each channel (CDTY in the PWM_CDTYx register). Writing in PWM_CDTYx
	 register is possible while the channel is disabled. After validation of the channel, the user must use
	 PWM_CDTYUPDx register to update PWM_CDTYx as explained below*/
	 F_PWM_CDTY0=0xfffu;
	 F_PWM_CDTY1=0xfffu;
	 F_PWM_CDTY2=0xfffu;
	 F_PWM_CDTY3=0xfffu;
	
	 /*Configuration of the dead-time generator for each channel (DTH and DTL in PWM_DTx) if enabled (DTE bit in the
	 PWM_CMRx). Writing in the PWM_DTx register is possible while the channel is disabled. After validation of the
	 channel, the user must use PWM_DTUPDx register to update PWM_DTx*/
	 /*Selection of the synchronous channels (SYNCx in the PWM_SCM register)*/
	 /*This register can only be written if bits WPSWS2 and WPHWS2 are cleared in the �PWM Write Protection Status Register�*/
	 //F_PWM_SCM|=SYNC0|SYNC1|SYNC2|SYNC3;
	 /*Selection of the moment when the WRDY flag and the corresponding PDC transfer request are set (PTRM and
	 PTRCS in the PWM_SCM register)*/
	 F_PWM_SCM&= ~UPDM0;
	 F_PWM_IER1|= CHID0| CHID1| CHID2| CHID3;
	 F_PWM_IER2|= WRDY;
	 
	 /*Enable PWM Channels*/
	 F_PWM_ENA|= CHID0| CHID1| CHID2| CHID3;
	 
};
int main(void)
{

/*Variables*/
int adcWert;
int adcWertvorher;
int status; /* 0= Stillstand, 1= Vorw�rts, 2= R�ckw�rts, ...weitere Ausnahmen (Bremsen, Beschleunigen)*/
int a_vor;
int a_rueck;


/*Init*/
adc_init();
pwm();

    while (1) 
    {
        adc_start_single();
		adcWert= adc_conversion_read();
		
		//for(int i=0;i<100;i++){
			//
		//}
		if(adcWert<0x7d0){
			/*Fahre r�ckw�rts*/
			/*PWM RW*/
		F_PWM_CDTYUPD0=adcWert;
		F_PWM_CDTYUPD1=adcWert;
		F_PWM_CDTYUPD2=0xffff;
		F_PWM_CDTYUPD3=0xffff;
		
					F_PWM_SCUC|= UPDULOCK;/*Enable update of duty cycle, period time and death time after current period, when UPDM==0*/
										
					F_WDT_CR =0xA5000000;/*Key A5*/
					F_WDT_CR =0xA5000001;/*und ResetPin1 auf 1 gesetzt*/
		}
			else if (adcWert>0x7d0){/*Fahre vorw�rts*/
			/*PWM VW*/
			F_PWM_CDTYUPD0=0xffff;
			F_PWM_CDTYUPD1=0xffff;
			F_PWM_CDTYUPD2=adcWert;
			F_PWM_CDTYUPD3=adcWert;
			
			F_PWM_SCUC|= UPDULOCK;/*Enable update of duty cycle, period time and death time after current period, when UPDM==0*/
			
			F_WDT_CR =0xA5000000;/*Key A5*/
			F_WDT_CR =0xA5000001;/*und ResetPin1 auf 1 gesetzt*/
			}
			
			else{F_PWM_CDTYUPD0=0xfffu;
			F_PWM_CDTYUPD1=0xfffu;
			F_PWM_CDTYUPD2=0xfffu;
			F_PWM_CDTYUPD3=0xfffu;
			
			F_PWM_SCUC|= UPDULOCK;/*Enable update of duty cycle, period time and death time after current period, when UPDM==0*/
			
			F_WDT_CR =0xA5000000;/*Key A5*/
			F_WDT_CR =0xA5000001;/*und ResetPin1 auf 1 gesetzt*/
			/*Stillstand*/
			
			adcWertvorher= adcWert;
			
		};
		
    }
}
