/*
 * names_bms.h
 *
 * Created: 14.01.2016 15:48:27
 *  Author: b3
 */ 


#ifndef NAMES_BMS_H_
#define NAMES_BMS_H_

#define DSPL_MAX 20
#define SPI_DSPL_WAIT 1370

#define LED_STAT0 10
#define LED_STAT1 11
#define LED_STAT2 22
#define LED_STAT3 23

#define CV1		0x00
#define CV2		0x01
#define CV3		0x02
#define CV4		0x03
#define CV5		0x04
#define CV6		0x05
#define ADV1	0x06
#define ADV2	0x07
#define ADV3	0x08
#define ADV4	0x09
#define ADV5	0x0A
#define ADV6	0x0B

#define SLF_TST	0x0C
#define CTRL1	0x0D
#define CTRL2	0x0E
#define COV		0x0F
#define CUV		0x10
#define ADOV	0x11
#define ADUV	0x12
#define	ALRT	0x13
#define CBLC	0x14
#define CBT1	0x15
#define CBT2	0x16
#define CBT3	0x17
#define CBT4	0x18
#define CBT5	0x19
#define CBT6	0x1A
#define PDT		0x1B
#define READ	0x1C
#define _CNVST	0x1D

#define DSY_CHN_REG_RDBK	0
#define	INC_DEV_ADR			1
#define LCK_DEV_ADR			2
#define	THRMSTR_RESISTOR	3
#define	SET_AQU_TIM			5
#define SFTW_RST			7
#define PD_FRMT				8
#define	CNV_AV				9
#define	CNV_STRT_FRMT		11
#define	CNV_RD_RSLT			12
#define	CNV_INPT_SLCT		14

#define CB1		0x04
#define CB2		0x08
#define CB3		0x10
#define CB4		0x20
#define CB5		0x40
#define CB6		0x80
#define CBOFF	0x00
#define CBON	0xFF

#define CB_ON	1
#define CB_OFF	0

#endif /* NAMES_BMS1.0V1.0_H_ */