/*
 * Names_samd10d14am.h
 *
 * Created: 22.12.2015 17:11:19
 *  Author: b3
 */ 


#ifndef NAMES_SAMD10D14AM_H_
#define NAMES_SAMD10D14AM_H_

/*Define Power Mangement*/
#define F_PM_CTRL		*((volatile unsigned char *) 0x40000400)
#define F_PM_SLEEP		*((volatile unsigned char *) 0x40000401)
#define F_PM_EXTCTRL	*((volatile unsigned char *) 0x40000402)
#define F_PM_CPUSEL		*((volatile unsigned char *) 0x40000408)
#define F_PM_APBASEL	*((volatile unsigned char *) 0x40000409)
#define F_PM_APBBSEL	*((volatile unsigned char *) 0x4000040A)
#define F_PM_APBCSEL	*((volatile unsigned char *) 0x4000040B)
#define F_PM_AHBMASK	*((volatile unsigned int *) 0x40000414)
#define F_PM_APBAMASK	*((volatile unsigned int *) 0x40000418)
#define F_PM_APBBMASK	*((volatile unsigned int *) 0x4000041C)
#define F_PM_APBCMASK	*((volatile unsigned int *) 0x40000420)
#define F_PM_INTENCLR	*((volatile unsigned char *) 0x40000434)
#define F_PM_INTENSET	*((volatile unsigned char *) 0x40000435)
#define F_PM_INTFLAG	*((volatile unsigned char *) 0x40000436)
#define F_PM_RCCAUSE	*((volatile unsigned char *) 0x40000438)

/*Define Uhren*/
#define F_GCLK_CTRL		*((volatile unsigned char *) 0x40000C00)
#define F_GCLK_STATUS	*((volatile unsigned char *) 0x40000C01)
#define F_GCLK_CLKCTRL	*((volatile unsigned short int *) 0x40000C02)
#define F_GCLK_GENCTRL	*((volatile unsigned int *) 0x40000C04)
#define F_GCLK_GENDIV	*((volatile unsigned int *) 0x40000C08)

/*Define PortA*/
#define F_PIOA_DIR		*((volatile unsigned int *) 0x41004400)
#define F_PIOA_DIRCLR	*((volatile unsigned int *) 0x41004404)
#define F_PIOA_DIRSET	*((volatile unsigned int *) 0x41004408)
#define F_PIOA_DIRTGL	*((volatile unsigned int *) 0x4100440C)
#define F_PIOA_OUT		*((volatile unsigned int *) 0x41004410)
#define F_PIOA_OUTCLR	*((volatile unsigned int *) 0x41004414)
#define F_PIOA_OUTSET	*((volatile unsigned int *) 0x41004418)
#define F_PIOA_OUTTGL	*((volatile unsigned int *) 0x4100441C)
#define F_PIOA_IN		*((volatile unsigned int *) 0x41004420)
#define F_PIOA_CTRL		*((volatile unsigned int *) 0x41004424)
#define F_PIOA_WRCONFIG	*((volatile unsigned int *) 0x41004428)

#define F_PIOA_PINCFG10	*((volatile unsigned int *) 0x4100444A)
#define F_PIOA_PINCFG11	*((volatile unsigned int *) 0x4100444B)
#define F_PIOA_PINCFG22	*((volatile unsigned int *) 0x41004456)
#define F_PIOA_PINCFG23	*((volatile unsigned int *) 0x41004457)

/*Define Pin Konfiguration*/
#define F_PIOA_PINCFG16	*((volatile unsigned int *) 0x41004450) /*Push2*/
#define F_PIOA_PINCFG15	*((volatile unsigned int *) 0x4100444F)/*Push1*/
#define F_PIOA_PINCFG14	*((volatile unsigned int *) 0x4100444E)/*Push0*/

/*Define Testzwecke*/
#define F_PIOA_DIR6		*((volatile unsigned int *) 0x60000000)
#define F_PIOA_OUT6		*((volatile unsigned int *) 0x60000010)

/*Define PAC Kontroller*/
#define F_PAC_WPCLR0		*((volatile unsigned int *) 0x40000000)
#define F_PAC_WPCLR1		*((volatile unsigned int *) 0x41000000)
#define F_PAC_WPCLR2		*((volatile unsigned int *) 0x42000000)

/*Define EIC Kontroller*/

#define F_EIC_CTRL			*((volatile unsigned char *) 0x40001800)
#define F_EIC_STATUS		*((volatile unsigned char *) 0x40001801)
#define F_EIC_NMICTRL		*((volatile unsigned char *) 0x40001802)
#define F_EIC_NMIFLAG		*((volatile unsigned char *) 0x40001803)
#define F_EIC_EVCTRL		*((volatile unsigned int *) 0x40001804)
#define F_EIC_INTENCLR		*((volatile unsigned int *) 0x40001808)
#define F_EIC_INTENSET		*((volatile unsigned int *) 0x4000180C)
#define F_EIC_INTFLAG		*((volatile unsigned int *) 0x40001810)
#define F_EIC_WAKEUP		*((volatile unsigned int *) 0x40001814)
#define F_EIC_CONFIG		*((volatile unsigned int *) 0x40001818)

/*Define SERCOM0 SPI Kontroller*/

#define F_SERC_CTRLA		*((volatile unsigned int *) 0x42000800)
#define F_SERC_CTRLB		*((volatile unsigned int *) 0x42000804)
#define F_SERC_BAUD			*((volatile unsigned char *) 0x4200080C)
#define F_SERC_INTENCLR		*((volatile unsigned char *) 0x42000814)
#define F_SERC_INTENSET		*((volatile unsigned char *) 0x42000816)
#define F_SERC_INTFLAG		*((volatile unsigned char *) 0x42000818)
#define F_SERC_STATUS		*((volatile unsigned short *) 0x4200081A)
#define F_SERC_SYNCBUSY		*((volatile unsigned int *) 0x4200081C)
#define F_SERC_ADDR			*((volatile unsigned int *) 0x42000824)
#define F_SERC_DATA			*((volatile unsigned char *) 0x42000828)
#define F_SERC_DBGCTRL		*((volatile unsigned char *) 0x42000830)

/*Define NVIC Kontroller*/
#define F_NVIC_ISER			*((volatile unsigned int *) 0xE000E100)


#endif /* NAMES_SAMD10D14AM_H_ */