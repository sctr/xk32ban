/*
 * BMS_0.1v0.1.c
 *
 * Created: 20.11.2015 21:18:48
 * Author : Scofield
 */ 
#include<stdio.h>
#include <stdbool.h>
#include "system_samd10.h"
#include "names_m0plus.h"
#include "oled_dspl.h"
#include "spi_m0plus.h"

#define DSPL 0
#define BMS 1
#define DATA 0
#define COMMAND 1
#define FREI 0
#define BELEGT 1


#define EXIT_FAILURE -1


/*GLOBALE STATUS VARIABLEN*/
/*END GLOBALE STATUS VARIABLEN*/

/*END GLOBALE VARIABLEN*/
char Zl1[]="C1:";
char Zl2[]="C2:";
char Zl3[]="C3:";
char Zl4[]="C4:";
char Zl5[]="C5:";
char Zl6[]="C6:";
char AV[]="AV:";
char ST[]="ST:";


/*END GLOBALE VARIABLEN*/

/*FUNKTIONEN/ SUBS*/




/*ENDE FUNKTIONEN/ SUBS*/



/*INTERRUPT HANDLER*/
void EIC_Handler(void){
	F_PIOA_OUTTGL=0x00800000;//toggel pin23

	F_PIOA_OUTTGL|=(0x1u<<27);//xlator toggeln
	
	F_EIC_INTFLAG|=(0x1<<0)|(0x1<<1);
}
void NMI_Handler(void){
	F_EIC_NMIFLAG|=(0x1<<0);
}

//void SERCOM0_Handler(void){
	//if (F_SERC_INTFLAG & (0x1u<<1)){
		//stat_spi = 0;
		//F_PIOA_OUT|=(0x1u<<8);
		//F_SERC_INTFLAG|=(0x1u<<1);
	//}
	//if(F_SERC_INTFLAG & (0x1u<<2)){
		//receive_data_spi=F_SERC_DATA;//lese empfangene Daten (Intflag INTRXC wird automatisch gel�scht)
	//}
//}
	
/*END INTERRUPT HANDLER*/

int main(void){
	
	/*UHREN EINSTELLEN*/
	//EICi
		
	F_GCLK_GENDIV=0x00000000;
	F_GCLK_GENCTRL=0x00010600;
	F_GCLK_CLKCTRL=0x4005; /*Generische Uhr (OSC8M) aktivieren, Generische Uhr erlauben*/
	//SERCOM0
	F_PM_APBCMASK|=(0x1u<<2);/*Schaltet die SERCOM0 Uhr ein*/
	F_GCLK_GENDIV=0x00000001;
	F_GCLK_GENCTRL=0x00010601;
	F_GCLK_CLKCTRL=0x410E;
	/*ENDE UHREN EINSTELLEN*/
	
	/*INTERRUPTS FREISTELLEN*/
	F_NVIC_ISER|=(0x1u<<4)/*|(0x1u<<9)*/; /*Setzt EICi, SERCOM0 Interrupts frei*/
	/*ENDE INTERRUPTS FREISTELLEN*/

		
	/*PORTA EINSTELLEN*/
	/*PIN 10,11,22,23 f�r LED als sink Ausgang konfigurieren*/
	/*PIN 4,5,6,8 f�r SPI als Ausgang konfigurieren*/
	/*Pin 27 als Ausgang f�r EN_Enable (xlator + vcc isoconverter)*/
	/*Pin 3 als Ausgang f�r _CNVST Conversion start*/
    F_PIOA_DIR|=(0x1u<<10)|(0x1u<<11)|(0x1u<<22)|(0x1u<<23)|(0x1u<<4)|(0x1u<<5)|(0x1u<<6)|(0x1u<<8)|(0x1u<<27)|(0x1u<<3);
	F_PIOA_OUTSET|=(0x1u<<8)|(0x1u<<6)|(0x1u<<5);//_SS Pin f�r Display und BMS hoch ziehen
	F_PIOA_OUTSET|=(0x1u<<27);//xlator an
	//F_PIOA_OUT&=~(0x1u<<27);//xlator aus
	F_PIOA_OUT|=(0x1u<<3);//_CNVST an
	
	/*Pin 14,15,16 f�r Taster als Eingang konfigurieren*/
	/*Pin 7 f�r SPI als Eingang konfigurieren*/
	F_PIOA_DIR &= ~((0x1u<<14)|(0x1u<<15)|(0x1u<<16)|(0x1u<<7));
		
	/*Pin 14,15,16 f�r Taster hoch setzen*/
	F_PIOA_OUTSET|= (0x1u<<14)|(0x1u<<15)|(0x1u<<16);
	
	/*Einstellungen der Pins werden mit einer Pinmaske gesetzt 
	Pinmasken sind Halbw�rter, d.h. (1<<31) selektiert die oberen 16 Pins
	~&(1<<31) selektiert die unteren 16 Pins.
	ACHTUNG: Die Pins k�nnen nur beschrieben werden wenn (0x1u<<30) f�r PINConfig und (0x1u<<28) f�r WRConfig
	F_PIOA_PINCFG werden nicht beschrieben, das erledigt die Pinmask von WRCONFIG*/
	
	/*Taster Pins konfigurieren*/
	F_PIOA_WRCONFIG|=(0x1u<<14)|(0x1u<<15)|(0x1u<<16)|(0x1u<<17)|(0x1u<<18)|(0x1u<<30);//PULLEN INEN Pins 14, 15
	F_PIOA_WRCONFIG|=(0x1u<<0)|(0x1u<<16)|(0x1u<<17)|(0x1u<<18)|(0x1u<<30)|(0x1u<<31);//PULLEN INEN Pins 16
	/*SPI Pins konfigurieren*/
	F_PIOA_WRCONFIG|=(0x1u<<4)|(0x1u<<5)|(0x1u<<7)|(0x1u<<16)|(0x3u<<24)|(0x1u<<28)|(0x1u<<30);//Multiplexing Pins 4,5,7, Peripher D, MISO MOSI CLK (kein SS, weil Harware seitig selektiert wird) 
	
	/*ENDE PORTA EINSTELLEN*/


	/*EIC EINSTELLEN*/
	F_EIC_CONFIG|=(0x2<<0)|(0x2<<4);/*Fallende Flanken Pr�fung, Keine Filter*/
	F_EIC_INTENSET|=(0x1<<0)|(0x1<<1);/*EXTINT 0 und 1 an Pin 15 und 16 als externen Interrupt gesetzt*/
		
	/*NMI Initialisieren*/
	F_EIC_NMICTRL|=(0x2<<0);/*Fallende Flanken Pr�fung, Keine Filter*/
	
	/*EICi erlauben*/
	F_EIC_CTRL|=(0x1<<1);
 
	/*END EIC EINSTELLEN*/
	
	/*SERCOM0 SPI EINSTELLEN*/
	F_SERC_CTRLA|=(0x3u<<2)|(0x3u<<20)|(0x1u<<28)|(0x1u<<29);				//Mastermode SPI, Pad Konfiguration, optional cpol, cpha
	//F_SERC_CTRLA|=(0x1u<<16);						//Andere Pad Konfiguration f�r MOSI, SCK, SS Verteilung
	//F_SERC_CTRLB|=(0x1u<<0);						//Bleibt Null f�r 8 Karakteren; unkommentieren, um 9 Karaktere mit einer �bertragung zu senden
	//F_SERC_CTRLA|= (0x1u<<30);					//MSB wird zuerst gesendet=0; um LSB zuerst zu senden=1
	F_SERC_BAUD=0xffu;								//Geschwindigkeitsrate der SPI Uhr (255)
	F_SERC_INTENSET|=(0x1u<<0);						//Interrupt erlauben, der anzeigt, wenn das DATA Register wieder leer ist
	F_SERC_CTRLB|=(0x1u<<13)|(0x1u<<17);			//Hardware Slave Auswahl, Empfangsregister zulassen
	F_SERC_INTENCLR|=(0x1u<<0);						//erlaubt keine Interrupts, wenn DATA leer meldet
	//F_SERC_INTENSET|=(0x1u<<1);					//Interrupt erlauben (wenn eine �bertragung erfolgreich war)
	F_SERC_INTENSET&=~(0x1u<<1);					//SERCOM Interrupt verbieten
	F_SERC_CTRLA|=(0x1u<<1);						//SPI Interface anschalten
	/*END SERCOM0 SPI EINSTELLEN*/
		
	dspl_init();


	dspl_clr();//clear display
	
	dspl_satz(Zl1,1,1);
	dspl_satz(Zl2,2,1);
	dspl_satz(Zl3,3,1);
	dspl_satz(Zl4,4,1);
	dspl_satz(Zl5,1,10);
	dspl_satz(Zl6,2,10);
	dspl_satz(AV,3,10);
	dspl_satz(ST,4,10);
		
	/*Software Reset*/
	tx_spi(0x01D2B412, BMS, DATA);
	tx_spi(0x01C2B6E2, BMS, DATA);
		
	/*------------------BMS IC initialisieren------------------------------------------*/
	
	tx_spi(0x01C2B6E2,BMS,DATA);		//Initializieren aller Ics
	bms_write(0x00,0x1C,0x38,1);		//Control Register Lower Byte auf Read Register schreiben
		for(int i = 0; i<1000; i++);	//warten
	trx_spi(0xF800030A,BMS,DATA);		//Dummy Wert auf MOSI um gleichzeitig Werte �ber MISO zur�ck zu erhalten
		for(int i = 0; i<1000; i++);	//warten
		//
	//bms_write(0x00,CBLC,CBON,0);
	//
	//bms_write(0x00,READ,(CBLC<<2),0);	
	//trx_spi(0xF800030A,BMS,DATA);
	//int empfang = bms_read(0x00,CBLC);
	//dspl_zahl(empfang, 1, 1);
	//bms_write(0x00,CBLC,CBOFF,0);
	//for(int i = 0; i<1000; i++);	//warten
	//empfang = bms_read(0x00,CBLC);
	//dspl_zahl(empfang, 1, 1);
	//empfang = bms_read(0x00,CBLC);
	////
	//
	//
	//bms_write(0x00,CBLC,CB4,0);
	//bms_write(0x00,READ,(CBLC<<2),0);
	//trx_spi(0xF800030A,BMS,DATA);
	//
	//bms_write(0x00,READ,(ALRT<<2),0);	
	//trx_spi(0xF800030A,BMS,DATA);
			
	
     while(1){
	/*------------------Alle Werte vom BMS IC auslesen(Spannung/Temperatur)------------*/
		 
		 //int v[6];
		 //
		//for(int i=0;i<6;i++){
		//bms_write(0x00,READ,((i+1)<<2),0);
		//v[i]=trx_spi(0xF800030A,BMS,DATA);
		//}
		
		
							  
		trx_spi(0x038011CA,BMS,DATA);		//Read Register den Wert 0x00 zuweisen um alle Spannungen/Temp zu erhalten
		trx_spi(0x01A0131A,BMS,DATA);		//Control Register D12-D15 0b0000 zum lesen aller Spannungen/Temp
		trx_spi(0x03A0546A,BMS,DATA);		//_CNVST Register 0x02 sodass eine Messung auf einer fallenden Flanke von _CNVST beginnt
		//_CNVST fallen lassen um Spannungen und Temperaturen zu lesen
		F_PIOA_OUT&=~(0x1u<<3);//_CNVST aus
		for(int i = 0; i<500; i++);	//_CNVST eine Weile auf GND lassen
		F_PIOA_OUT|=(0x1u<<3);//_CNVST an
		for(int i = 0; i<250; i++);	//_CNVST eine Weile auf GND lassen
		
		for(int i = 0; i<12; i++){
						
			uint32_t result_bms=0x00000000;
			uint8_t rcv_dev=0x0000;
			uint8_t rcv_ch=0x00;
			uint16_t rcv_dat=0x0000;
			bool rcv_ack=0;
	
			result_bms=trx_spi_cnvst(0xF800030A,BMS,DATA);		//Dummy Wert auf MOSI um gleichzeitig Werte �ber MISO zur�ck zu erhalten
	
			rcv_dev=(result_bms>>27)&0x1F;
			rcv_ch=(result_bms>>23)&0xF;
			rcv_dat=(result_bms>>11)&0xFFF;
			rcv_ack=(result_bms>>10)&0b1;
			if(rcv_ch<4){
				dspl_zahl(rcv_dat+1000,rcv_ch+1,5);
			}
			else if(rcv_ch<6){
				dspl_zahl(rcv_dat+1000,rcv_ch-3,14);
			}
		};
	
    }

}

